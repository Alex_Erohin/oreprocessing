﻿using Microsoft.AspNetCore.Mvc;
using OreProcessing.Models.DTO;
using OreProcessing.Models.ViewModel;
using OreProcessing.Services;
using System.Collections.Generic;

namespace OreProcessing.Controllers.API
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SchedulerController : ControllerBase
    {
        private readonly ISchedulerServiceFactory _schedulerServiceFactory;

        public SchedulerController(ISchedulerServiceFactory schedulerServiceFactory)
        {
            _schedulerServiceFactory = schedulerServiceFactory;
        }

        [HttpPost]
        public string ParseFiles([FromBody]ParseFilesViewModel model)
        {
           return _schedulerServiceFactory.Create(true).ParseFiles(model.MachineToolsFile.Path, model.NomenclatursFile.Path,
                model.PartiesFile.Path, model.TimesFile.Path);
        }

        [HttpPost]
        public IEnumerable<ScheduleItem> Create([FromQuery]string cacheKey, [FromQuery]bool isQueue, [FromQuery]int partiesCount)
        {
            return _schedulerServiceFactory.Create(isQueue).Create(cacheKey, partiesCount);
        }
    }
}