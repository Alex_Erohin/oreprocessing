﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OreProcessing.Models.DTO;
using OreProcessing.Services;

namespace OreProcessing.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        private readonly IUploadService _uploadService;

        public UploadController(IUploadService uploadService)
        {
            _uploadService = uploadService;
        }

        [HttpPost]
        public async Task<UploadFile> Post()
        {
            var files = HttpContext.Request.Form?.Files;
            var uploadFile = new UploadFile();

            if (files.Any())
            {
                uploadFile = await _uploadService.UploadFile(files.FirstOrDefault());
            }

            return uploadFile;
        }
    }
}