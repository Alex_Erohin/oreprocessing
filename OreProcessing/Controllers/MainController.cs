﻿using Microsoft.AspNetCore.Mvc;

namespace OreProcessing.Controllers
{
    public class MainController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
