﻿namespace OreProcessing.Models.DTO
{
    public class MachineTool
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
