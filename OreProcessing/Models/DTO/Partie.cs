﻿namespace OreProcessing.Models.DTO
{
    public class Partie
    {
        public int Id { get; set; }
        public int NomenclatureId { get; set; }
    }
}
