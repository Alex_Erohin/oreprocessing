﻿using Newtonsoft.Json;
using System;

namespace OreProcessing.Models.DTO
{
    public class ScheduleItem : ICloneable
    {
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }
        [JsonIgnore]
        public MachineTool MachineTool { get; set; }
        [JsonIgnore]
        public Nomenclature Nomenclature { get; set; }
        [JsonIgnore]
        public Partie Partie { get; set; }

        #region For UI chart
        public DateTime StartDate => DateTime.Now.Date.Add(Start);
        public DateTime EndDate => DateTime.Now.Date.Add(End);
        public string MachineToolName => MachineTool?.Name;
        public string NomenclatureName => Nomenclature?.Name;
        public int PartieNumber => Partie.Id;
        #endregion

        public object Clone()
        {
            MachineTool machineTool = new MachineTool { Id = MachineTool.Id, Name = MachineTool.Name };
            Nomenclature nomenclature = new Nomenclature { Id = Nomenclature.Id, Name = Nomenclature.Name };
            Partie partie = new Partie { Id = Partie.Id, NomenclatureId = Partie.NomenclatureId };

            return new ScheduleItem { 
                Start = Start, 
                End = End,
                MachineTool = machineTool,
                Nomenclature = nomenclature,
                Partie = partie
            };
        }
    }
}
