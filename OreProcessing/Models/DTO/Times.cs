﻿namespace OreProcessing.Models.DTO
{
    public class Times
    {
        public int NomenclatureId { get; set; }
        public int MachineToolId { get; set; }
        public int OperationTime { get; set; }
    }
}
