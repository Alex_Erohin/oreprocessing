﻿namespace OreProcessing.Models.DTO
{
    public class UploadFile
    {
        public string Path { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
    }
}
