﻿namespace OreProcessing.Models.DTO
{
    public enum UploadFileType
    {
        MachineTool, Nomenclature, Parties, Times
    }
}
