﻿using OreProcessing.Models.DTO;
using System.Collections.Generic;

namespace OreProcessing.Models.Domain
{
    public class AggregateModel
    {
        public List<MachineTool> MachineTools { get; set; }
        public List<Nomenclature> Nomenclatures { get; set; }
        public List<Partie> Parites { get; set; }
        public List<Times> Times { get; set; }
    }
}
