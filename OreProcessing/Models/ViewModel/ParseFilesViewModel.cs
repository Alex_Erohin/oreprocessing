﻿using OreProcessing.Models.DTO;

namespace OreProcessing.Models.ViewModel
{
    public class ParseFilesViewModel
    {
        public UploadFile MachineToolsFile { get; set; }
        public UploadFile NomenclatursFile { get; set; }
        public UploadFile PartiesFile { get; set; }
        public UploadFile TimesFile { get; set; }
    }
}
