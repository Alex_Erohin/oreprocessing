﻿using Microsoft.Extensions.Caching.Memory;
using OreProcessing.Models.Domain;
using OreProcessing.Models.DTO;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace OreProcessing.Services
{
    /// <summary>
    /// Алгоритм расчета расписания на основе случайного направленного поиска с разбиением на итерации
    /// </summary>
    public class SchedulerDirectionalSearchService : SchedulerService
    {
        private static object _obj = new { };
        public SchedulerDirectionalSearchService(IMemoryCache memoryCache) : base(memoryCache) { }

        public override IEnumerable<ScheduleItem> Create(string cacheKey, int partiesCount)
        {
            if (MemoryCache.TryGetValue(cacheKey, out AggregateModel model))
            {
                // Получаем все возможные шаги загрузки оборудования
                var steps = model.Parites.ToDictionary(p => p.Id,
                    p => model.MachineTools
                        .Where(m => model.Times.Any(t => t.MachineToolId == m.Id && t.NomenclatureId == p.NomenclatureId)));

                // Разбиваем на итерации
                var iterations = new Dictionary<int, Dictionary<int, IEnumerable<MachineTool>>>();
                for (int i = 0; i < Math.Ceiling(steps.Count() / (decimal)partiesCount); i++)
                {
                    iterations.Add(i, steps.Skip(i* partiesCount).Take(partiesCount).ToDictionary(pair => pair.Key, pair => pair.Value));
                }
                List<ScheduleItem> result = new List<ScheduleItem>();

                foreach(var iteration in iterations)
                {
                    var r = GetFastScheduleItems(iteration.Value, iteration.Key, model, result, partiesCount);
                    result = r.ToList();
                }

                return result.OrderBy(q => q.PartieNumber).ToList();
            }
            else throw new KeyNotFoundException("В кэше не найдены значения элементов из загруженных файлов");
        }

        private List<ScheduleItem> GetFastScheduleItems(Dictionary<int, IEnumerable<MachineTool>> steps, 
            int iterationId, AggregateModel model, List<ScheduleItem> fillSchedules, int partiesCount)
        {
            var minTime = TimeSpan.FromHours(72);
            var queues = new List<Queue<int>>();
            var result = new List<ScheduleItem>();
            var parties = model.Parites.Skip(iterationId * partiesCount).Take(partiesCount).ToList();
            var counter = 0;

            // Заполняем все возможные последовательности загрузки оборудования в итерации
            foreach (var step in steps)
            {
                var machineCounter = 0;
                var startQueues = queues.Select(q => new Queue<int>(q)).ToList();

                foreach (var machine in step.Value)
                {
                    if (counter == 0)
                    {
                        var queue = new Queue<int>();
                        queue.Enqueue(machine.Id);
                        queues.Add(queue);
                    }
                    else
                    {
                        if (machineCounter == 0)
                        {
                            queues = queues
                            .Select(q =>
                            {
                                q.Enqueue(machine.Id);
                                return q;
                            }).ToList();
                        }
                        else
                        {
                            var newQueues = startQueues.Select(q =>
                            {
                                var qq = new Queue<int>(q);
                                qq.Enqueue(machine.Id);
                                return qq;
                            }).ToList();
                            queues = queues.Concat(newQueues).ToList();
                        }
                    }

                    machineCounter++;
                }

                counter++;
            }

            Parallel.ForEach(queues, queue =>
            {
                var scheduleList = new List<ScheduleItem>(fillSchedules.Select(s => s.Clone() as ScheduleItem).ToList());
                Partie prevPartie = !scheduleList.Any() ? null :
                    scheduleList.OrderByDescending(s => s.Partie.Id).FirstOrDefault().Partie;
                var minStart = TimeSpan.FromSeconds(0);
                int index = 0;

                foreach (var machineId in queue.ToImmutableList())
                {
                    queue.Dequeue();

                    // Поиск времени загрузки предыдущей партии
                    if (prevPartie != null)
                    {
                        minStart = scheduleList.FirstOrDefault(s => s.Partie.Id == prevPartie.Id).Start;
                    }

                    // Поиск окончания последней загрузки
                    var lastEnd = scheduleList.Where(s => s.MachineTool.Id == machineId)
                        .OrderByDescending(v => v.Partie.Id).FirstOrDefault()?.End;
                    var startTime = minStart > lastEnd.GetValueOrDefault() ? minStart : lastEnd.GetValueOrDefault();
                    var partie = parties[index];

                    scheduleList.Add(new ScheduleItem
                     {
                         Start = startTime,
                         End = startTime.Add(TimeSpan.FromMinutes(
                            model.Times.FirstOrDefault(t => t.MachineToolId == machineId
                                && t.NomenclatureId == partie.NomenclatureId).OperationTime)),
                         MachineTool = model.MachineTools.FirstOrDefault(m => m.Id == machineId),
                         Nomenclature = model.Nomenclatures.FirstOrDefault(n => n.Id == partie.NomenclatureId),
                         Partie = partie
                     });

                    prevPartie = partie;

                    index++;
                }

                lock (_obj)
                {
                    var maxStart = scheduleList.Max(s => s.Start);
                    if (maxStart < minTime)
                    {
                        minTime = maxStart;
                        result = scheduleList;
                    }
                }
            });

            return result;
        }
    }
}
