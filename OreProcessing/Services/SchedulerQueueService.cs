﻿using Microsoft.Extensions.Caching.Memory;
using OreProcessing.Models.Domain;
using OreProcessing.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OreProcessing.Services
{
    /// <summary>
    /// Алгоритм расчета расписания на основе первого доступного оборудования в очереди
    /// </summary>
    public class SchedulerQueueService : SchedulerService
    {
        public SchedulerQueueService(IMemoryCache memoryCache) : base(memoryCache) { }

        public override IEnumerable<ScheduleItem> Create(string cacheKey, int partiesCount)
        {
            if (MemoryCache.TryGetValue(cacheKey, out AggregateModel model))
            {
                var queues = model.MachineTools.ToDictionary(m => m.Id, m => new List<ScheduleItem>());
                var minStart = TimeSpan.FromSeconds(0);
                Partie prevPartie = null;

                foreach(var partie in model.Parites.OrderBy(p => p.Id))
                {
                    // Поиск времени загрузки предыдущей партии
                    if (prevPartie != null)
                    {
                        minStart = queues.SelectMany(g => g.Value).FirstOrDefault(s => s.Partie.Id == prevPartie.Id).Start;
                    }

                    // Поиск возможного для загрузки оборудования
                    var targetMachine = queues
                        .Where(g => model.Times.Any(t => t.MachineToolId == g.Key && t.NomenclatureId == partie.NomenclatureId))
                        .OrderBy(q => q.Value.OrderByDescending(qu => qu.Start).FirstOrDefault()?.Start)
                        .FirstOrDefault();

                    // Поиск окончания последней загрузки
                    var lastEnd = targetMachine.Value.OrderByDescending(v => v.Partie.Id).FirstOrDefault()?.End;
                    var startTime = minStart > lastEnd.GetValueOrDefault() ? minStart : lastEnd.GetValueOrDefault();

                    targetMachine.Value.Add(new ScheduleItem
                    {
                        Start = startTime,
                        End = startTime.Add(TimeSpan.FromMinutes(
                            model.Times.FirstOrDefault(t => t.MachineToolId == targetMachine.Key
                                && t.NomenclatureId == partie.NomenclatureId).OperationTime)),
                        MachineTool = model.MachineTools.FirstOrDefault(m => m.Id == targetMachine.Key),
                        Nomenclature = model.Nomenclatures.FirstOrDefault(n => n.Id == partie.NomenclatureId),
                        Partie = partie
                    });

                    prevPartie = partie;
                }

                return queues.SelectMany(g => g.Value).OrderBy(q => q.PartieNumber).ToList();
            }
            else throw new KeyNotFoundException("В кэше не найдены значения элементов из загруженных файлов");
        }
    }
}
