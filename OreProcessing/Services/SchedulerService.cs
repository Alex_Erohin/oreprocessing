﻿using Microsoft.Extensions.Caching.Memory;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using OreProcessing.Models.Domain;
using OreProcessing.Models.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OreProcessing.Services
{
    public interface ISchedulerService
    {
        /// <summary>
        /// Парсинг загружаемых Excel-файлов
        /// </summary>
        string ParseFiles(string machineToolsFile, string nomenclatursFile, string partiesFile, string timesFile);

        /// <summary>
        /// Построение плана
        /// </summary>
        IEnumerable<ScheduleItem> Create(string cacheKey, int partiesCount);
    }

    public abstract class SchedulerService : ISchedulerService
    {
        protected readonly IMemoryCache MemoryCache;

        public SchedulerService(IMemoryCache memoryCache)
        {
            MemoryCache = memoryCache;
        }

        public virtual string ParseFiles(string machineToolsFile, string nomenclatursFile, string partiesFile, string timesFile)
        {
            var files = new Dictionary<UploadFileType, string>
            {
                { UploadFileType.MachineTool, machineToolsFile },
                { UploadFileType.Nomenclature, nomenclatursFile },
                { UploadFileType.Parties, partiesFile },
                { UploadFileType.Times, timesFile }
            };

            var cacheKey = Guid.NewGuid().ToString();
            var aggregate = new AggregateModel();

            // Распараллелим парсинг файлов, для оптимизации
            Parallel.ForEach(files, file =>
            {
                if (!File.Exists(file.Value))
                    throw new FileNotFoundException($"{GetFileTypeName(file.Key)} - файл не найден");

                var package = new ExcelPackage(new FileInfo(file.Value));
                ExcelWorksheet sheet = package.Workbook.Worksheets.First();

                switch (file.Key)
                {
                    case UploadFileType.MachineTool:
                        aggregate.MachineTools = MapFileRows<MachineTool>(sheet).ToList();
                        break;
                    case UploadFileType.Nomenclature:
                        aggregate.Nomenclatures = MapFileRows<Nomenclature>(sheet).ToList();
                        break;
                    case UploadFileType.Parties:
                        aggregate.Parites = MapFileRows<Partie>(sheet).ToList();
                        break;
                    case UploadFileType.Times:
                        aggregate.Times = MapFileRows<Times>(sheet).ToList();
                        break;
                }
            });

            // Сохраняем данные в кэш (обратимся к нему при составлении расписания)
            MemoryCache.Set(cacheKey, aggregate, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(8)
            });
            return cacheKey;
        }

        /// <summary>
        /// Маппинг данных из файла на DTO
        /// </summary>
        protected IEnumerable<T> MapFileRows<T>(ExcelWorksheet sheet)
            where T : class
        {
            for (int row = 2; row <= sheet.Dimension.End.Row; row++)
            {
                if (int.TryParse(sheet.Cells[row, 1].Value?.ToString(), out int id))
                {
                    if (typeof(T) == typeof(MachineTool))
                    {
                        yield return new MachineTool
                        {
                            Id = id,
                            Name = sheet.Cells[row, 2].Value?.ToString()
                        } as T;
                    }

                    if (typeof(T) == typeof(Nomenclature))
                    {
                        yield return new Nomenclature
                        {
                            Id = id,
                            Name = sheet.Cells[row, 2].Value?.ToString()
                        } as T;
                    }

                    if (typeof(T) == typeof(Partie) && int.TryParse(sheet.Cells[row, 2].Value?.ToString(), out int nomenclatureId))
                    {
                        yield return new Partie
                        {
                            Id = id,
                            NomenclatureId = nomenclatureId
                        } as T;
                    }

                    if (typeof(T) == typeof(Times) && int.TryParse(sheet.Cells[row, 2].Value?.ToString(), out int nomenclature)
                        && int.TryParse(sheet.Cells[row, 3].Value?.ToString(), out int time))
                    {
                        yield return new Times
                        {
                            MachineToolId = id,
                            NomenclatureId = nomenclature,
                            OperationTime = time
                        } as T;
                    }
                }
            }
        }

        protected Func<UploadFileType, string> GetFileTypeName = type =>
        {
            switch (type)
            {
                case UploadFileType.MachineTool:
                    return "Оборудование";
                case UploadFileType.Nomenclature:
                    return "Материалы";
                case UploadFileType.Parties:
                    return "Партии";
                case UploadFileType.Times:
                default:
                    return "Время обработки";
            }
        };

        public virtual IEnumerable<ScheduleItem> Create(string cacheKey, int partiesCount)
        {
            throw new NotImplementedException();
        }
    }
}
