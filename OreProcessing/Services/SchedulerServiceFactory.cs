﻿namespace OreProcessing.Services
{
    /// <summary>
    /// Фабрика создания инстанса ISchedulerService в зависимости от условия
    /// </summary>
    public interface ISchedulerServiceFactory
    {
        ISchedulerService Create(bool isQueue);
    }

    public class SchedulerServiceFactory : ISchedulerServiceFactory
    {
        private readonly ISchedulerService _schedulerDirectionalSearchService;
        private readonly ISchedulerService _schedulerQueueService;

        public SchedulerServiceFactory(
            SchedulerDirectionalSearchService schedulerDirectionalSearchService,
            SchedulerQueueService schedulerQueueService)
        {
            _schedulerQueueService = schedulerQueueService;
            _schedulerDirectionalSearchService = schedulerDirectionalSearchService;
        }

        public ISchedulerService Create(bool isQueue)
            => isQueue ? _schedulerQueueService : _schedulerDirectionalSearchService;
    }
}
