﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using OreProcessing.Models.DTO;
using System;
using System.IO;
using System.Threading.Tasks;

namespace OreProcessing.Services
{
    public interface IUploadService
    {
        /// <summary>
        /// Загрузка файлов на сервер
        /// </summary>
        Task<UploadFile> UploadFile(IFormFile formFile);
    }

    public class UploadService : IUploadService
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public UploadService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<UploadFile> UploadFile(IFormFile formFile)
        {
            var outputPath = Path.Combine(_hostingEnvironment.WebRootPath, "upload\\");
            var filePath = Path.Combine(outputPath, $"{Guid.NewGuid()}-{Path.GetFileName(formFile.FileName)}");

            if (!Directory.Exists(outputPath))
                Directory.CreateDirectory(outputPath);

            using (var stream = File.Create(filePath))
            {
                await formFile.CopyToAsync(stream);
            }

            return new UploadFile
            {
                FileName = formFile.FileName,
                Path = filePath,
                Description = $"Исходный размер: {Math.Round(formFile.Length / 1024m, 2)} kb"
            };
        }
    }
}
