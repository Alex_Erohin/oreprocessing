﻿import React from "react";
import ReactDom from "react-dom";
import "./main.scss";
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.material.blue.dark.css';
import { Button, FileUploader, Popup, RadioGroup, Slider } from "devextreme-react";
import {
    Chart, CommonSeriesSettings, Legend, SeriesTemplate, Animation, ArgumentAxis, Tick, Title, Series, ValueAxis, Label, Tooltip
} from 'devextreme-react/chart';
import notify from "devextreme/ui/notify";
import axios from 'axios';

(function (processMode, module) {

    class App extends React.PureComponent {
        constructor(props) {
            super(props);

            // refs
            this.popupRef = React.createRef();
            this.selectPopupRef = React.createRef();
            this.chartRef = React.createRef();
            this.sliderRef = React.createRef();
            this.sliderContainerRef = React.createRef();

            // binds
            this.onUploadStarted = this.onUploadStarted.bind(this);
            this.onUploadError = this.onUploadError.bind(this);
            this.onUploaded = this.onUploaded.bind(this);
            this.showPopup = this.showPopup.bind(this);
            this.onCreateSchedule = this.onCreateSchedule.bind(this);
            this.resetState = this.resetState.bind(this);
            this.errorHandler = this.errorHandler.bind(this);

            this.state = {
                machineToolsFile: null,
                nomenclatursFile: null,
                partiesFile: null,
                timesFile: null
            };

            this.algorithms = [
                { id: 0, subject: "Очередь доступного оборудования" },
                { id: 1, subject: "Направленный случайный поиск по итерациям" }
            ];

            this.selectAlgorithm = this.algorithms[0];
            this.partiesCount = 10;
        }

        get Popup() {
            return this.popupRef.current.instance;
        }

        get SelectPopup() {
            return this.selectPopupRef.current.instance;
        }

        get Chart() {
            return this.chartRef.current.instance;
        }

        get Slider() {
            return this.sliderRef.current.instance;
        }

        showPopup(title) {
            this.Popup.option("title", title);
            this.Popup.show();
        }

        onUploadStarted() {
            this.showPopup("Загрузка файла...");
        }

        onUploadError(e) {
            setTimeout(() => {
                this.Popup.hide();
                notify(JSON.parse(e.request.responseText).error, "error", 5000);
            }, 800);
        }

        onUploaded(e, key) {
            setTimeout(() => {
                this.Popup.hide();
                this.setState({
                    [key]: JSON.parse(e.request.responseText)
                });
            }, 800);
        }

        getFileControl(key, placeholder) {
            const fileuploaderSettings = {
                width: "100%",
                height: "100%",
                uploadMethod: "POST",
                uploadUrl: "/api/upload",
                labelText: "Файл xlsx",
                allowedFileExtensions: [".xlsx"],
                accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                onUploadStarted: this.onUploadStarted,
                onUploadError: this.onUploadError
            };

            return (
                <div style={{ marginTop: this.state[key] ? 10 : 0 }}>
                    {
                        !this.state[key] ?
                            <FileUploader selectButtonText={placeholder} name={key} {...fileuploaderSettings}
                                onUploaded={e => this.onUploaded(e, key)}
                            />
                            :
                            <React.Fragment>
                                <span>Файл загружен</span>
                                <Button icon="close" hint={`${placeholder} - удалить файл`} onClick={() => this.setState({
                                    [key]: null
                                })} />
                            </React.Fragment>
                    }
                </div>
            );
        }

        onCreateSchedule() {
            this.SelectPopup.hide();
            this.showPopup("Парсинг файлов...");
            axios.post('/api/scheduler/parseFiles', this.state)
                .then(e => {
                    setTimeout(() => {
                        this.Popup.option("title", "Создание расписания...");
                        axios.post(`/api/scheduler/create?cacheKey=${e.data}&isQueue=${this.selectAlgorithm.id === 0}&partiesCount=${this.partiesCount}`)
                            .then(d => {
                                this.Popup.hide();
                                this.Chart.option("dataSource", d.data);  
                            })
                            .catch(ex => this.errorHandler(ex));
                    }, 500);
                })
                .catch(ex => this.errorHandler(ex));
        }

        resetState() {
            this.setState({
                machineToolsFile: null,
                nomenclatursFile: null,
                partiesFile: null,
                timesFile: null
            });
        }

        errorHandler(e) {
            this.Popup.hide();
            notify({ ...e }.response.data.error || "Ошибка на сервере", "error", 5000);
        }

        tooltipRender(e) {
            return (
                <div>
                    <p>Партия: {e.point.data.partieNumber}</p>
                    <p>Диапазон дат:</p>
                    <p>{e.point.data.start} - {e.point.data.end}</p>
                </div>
            );
        }

        render() {
            return (
                <div className="app">
                    <div className="app-filter">
                        {
                            this.getFileControl("machineToolsFile", "Оборудование")
                        }
                        {
                            this.getFileControl("nomenclatursFile", "Материалы")
                        }
                        {
                            this.getFileControl("partiesFile", "Партии сырья")
                        }
                        {
                            this.getFileControl("timesFile", "Время обработки")
                        }
                        <div className="success-btn">
                            <Button text="Создать расписание" type="success"
                                disabled={!(this.state.machineToolsFile && this.state.nomenclatursFile && this.state.partiesFile
                                    && this.state.timesFile)}
                                onClick={() => {
                                    this.SelectPopup.show();
                                    this.sliderContainerRef.current.style.display = this.selectAlgorithm.id === 0 ? "none" : "block";
                                }}
                            />
                        </div>
                    </div>
                    <div className="app-diagram">
                        <Chart id="chart" barGroupPadding={2} rotated ref={this.chartRef}>
                            <ArgumentAxis>
                                <Tick visible />
                            </ArgumentAxis>
                            <ValueAxis valueType="datetime">
                                <Label format="HH:mm" />
                            </ValueAxis>
                            <Title text="Расписание загрузки партий" subtitle="для обработки сырья"
                            />
                            <CommonSeriesSettings
                                type="rangeBar"
                                argumentField="machineToolName"
                                rangeValue1Field="startDate"
                                rangeValue2Field="endDate"
                                barOverlapGroup="machineToolName"
                            >
                                <Label visible position="inside" customizeText={e => !e.index ? e.point.data.partieNumber || "0" : ""}
                                    showForZeroValues alignment="center"
                                />
                            </CommonSeriesSettings>
                            <Legend verticalAlignment="bottom" horizontalAlignment="center">
                                <Title text="Временная шкала" />
                            </Legend>
                            <SeriesTemplate nameField="nomenclatureName" />
                            <Animation enabled={false} />
                            <Tooltip enabled contentRender={this.tooltipRender} />
                        </Chart>
                    </div>
                    <Popup ref={this.popupRef} height={350} width={350}
                        dragEnabled={false} showCloseButton={false} >
                        <div className="loader" />
                    </Popup>
                    <Popup ref={this.selectPopupRef} height={350} width={350} dragEnabled={false} title="Алгоритм расчета">
                        <div className="algorithm">
                            <RadioGroup items={this.algorithms} defaultValue={this.selectAlgorithm} displayExpr="subject"
                                onValueChanged={e => {
                                    this.selectAlgorithm = e.value;
                                    this.sliderContainerRef.current.style.display = e.value.id === 0 ? "none" : "block";
                                }}
                            />
                            <div ref={this.sliderContainerRef}>
                                <span>Выберите шаг итерации (кол-во партий)</span>
                                <Slider min={10} max={15} ref={this.sliderRef} defaultValue={this.partiesCount} tooltip={{
                                    enabled: true,
                                    showMode: 'always',
                                    position: 'bottom'
                                }}
                                    onValueChanged={e => this.partiesCount = e.value}
                                />
                            </div>
                            <div style={{ textAlign: "center", marginTop:12 }}>
                                <Button text="Продолжить" onClick={this.onCreateSchedule} type="success" />
                            </div>
                        </div>
                    </Popup>
                </div>  
            );
        }
    }

    ReactDom.render(<App />, document.getElementById("app"));

    if (processMode === "development")
        module.hot.accept();
})(processMode, module);